function clock(){
    let date = new Date();
    let hours = date.getHours();
    let minites = date.getMinutes();
    let seconds = date.getSeconds();
    let session = "AM"
    if (hours == 0) {
        hours = 12;
    }
    if (hours > 12) {
        hours = 12;
        session = "PM";
    }
    
    hours = (hours < 10) ? "0"+hours : hours;
    minites = (minites < 10) ? "0"+minites : minites;
    seconds = (seconds < 10) ? "0"+seconds : seconds;
    seconds = (seconds < 10) ? "0"+seconds : seconds;

    let time = hours+" : "+ minites+" : "+seconds+" "+session;
    document.getElementById("root").innerText = time;
}

setInterval(clock,1000);