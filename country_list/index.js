let api_url = 'https://raw.githubusercontent.com/samayo/country-json/master/src/country-by-abbreviation.json';

fetch(api_url)
    .then(response => response.json())
    .then(data => {
        for (let item of data) {
            document.getElementById("demo").innerHTML += `<li type='1'>${item.country}</li>`;
        }
    })
    .catch(err => console.error(err));

