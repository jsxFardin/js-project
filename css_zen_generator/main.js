let container_div = document.createElement('div');
let intro = document.createElement('section');
let main_supporting = document.createElement('div');
let aside = document.createElement('aside');

// ----------zen intor section start---------
intro.setAttribute("id","zen-intro");
intro.setAttribute("class","intro");

// header or banner section 
let banner = document.createElement("header");
banner.setAttribute("role","banner");

let banner_h1 = document.createElement("h1");
let banner_h1_text = document.createTextNode("CSS Zen Garden");
banner_h1.appendChild(banner_h1_text);

let banner_h2 = document.createElement("h2");
let banner_h2_text_1 = document.createTextNode("The Beauty of");
let banner_h2_text_2 = document.createTextNode("Design");
let banner_h2_abbr = document.createElement("abbr");
banner_h2_abbr.setAttribute("title","Cascading Style Sheets");
let abbr_text = document.createTextNode(" CSS ");
banner_h2_abbr.appendChild(abbr_text);
banner_h2.appendChild(banner_h2_text_1);
banner_h2.appendChild(banner_h2_abbr)
banner_h2.appendChild(banner_h2_text_2);

banner.appendChild(banner_h1);
banner.appendChild(banner_h2);

// summary section
let summary = document.createElement("div");
summary.setAttribute("role","article");
summary.setAttribute("id","zen-summary");
summary.setAttribute("class","summary");

let summary_p1 = document.createElement("p");
let summary_p1_abbr = document.createElement("abbr");
summary_p1_abbr.setAttribute("title","Cascading Style Sheets");
let summary_p1_abbr_text1 = document.createTextNode("A demonstration of what can be accomplished through");
let summary_p1_abbr_text2 = document.createTextNode("-based design. Select any style sheet from the list to load it into this page.");
summary_p1_abbr.appendChild(abbr_text);
summary_p1.appendChild(summary_p1_abbr_text1);
summary_p1.appendChild(summary_p1_abbr);
summary_p1.appendChild(summary_p1_abbr_text2);


let summary_p2 = document.createElement("p");
let summary_p2_a1 = document.createElement("a");
summary_p2_a1.setAttribute("href","/examples/index");
summary_p2_a1.setAttribute("title","This page's source HTML code, not to be modified.");
let summary_p2_a1_text = document.createTextNode("html file");
summary_p2_a1.appendChild(summary_p2_a1_text);

let summary_p2_a2 = document.createElement("a");
summary_p2_a2.setAttribute("href","/examples/style.css");
summary_p2_a2.setAttribute("title","This page's sample CSS, the file you may modify.");
let summary_p2_a2_text = document.createTextNode("CSS file");
summary_p2_a2.appendChild(summary_p2_a2_text);

let summary_p2_text1 = document.createTextNode("Download the example ");
let summary_p2_text2 = document.createTextNode(" and ");
summary_p2.appendChild(summary_p2_text1);
summary_p2.appendChild(summary_p2_a1);
summary_p2.appendChild(summary_p2_text2);
summary_p2.appendChild(summary_p2_a2);

summary.appendChild(summary_p1);
summary.appendChild(summary_p2);

// preamble section
let preamble = document.createElement("div");
preamble.setAttribute("class","preamble");
preamble.setAttribute("id","zen-preamble");
preamble.setAttribute("role","article");

let preamble_h3 = document.createElement("h3");
let preamble_h3_Text = document.createTextNode("The Road to Enlightenment");
preamble_h3.appendChild(preamble_h3_Text);

let preamble_p1 = document.createElement("p");
let preamble_p1_text1 = document.createTextNode("Littering a dark and dreary road lay the past relics of browser-specific tags, incompatible ");
let preamble_p1_text2 = document.createTextNode("s, broken ");
let preamble_p1_text3 = document.createTextNode("support, and abandoned browsers.");
let preamble_abbr2 = document.createElement("abbr");
let preamble_abbr2_text = document.createTextNode("DOM");
preamble_abbr2.appendChild(preamble_abbr2_text);
banner_h2_abbr.appendChild(abbr_text);
preamble_abbr2.setAttribute("title","Document Object Model");
preamble_p1.appendChild(preamble_p1_text1);
preamble_p1.appendChild(preamble_abbr2);
preamble_p1.appendChild(preamble_p1_text2);
preamble_p1.appendChild(banner_h2_abbr);
preamble_p1.appendChild(preamble_p1_text3);

let preamble_p2 = document.createElement("p");
let premble_p2_text1= document.createTextNode("We must clear the mind of the past. Web enlightenment has been achieved thanks to the tireless efforts of folk like the ");
let premble_p2_text2= document.createTextNode(", and the major browser creators.");
let preamble_p2_abbr = document.createElement("abbr");
preamble_p2_abbr.setAttribute("title","Web Standards Project");
let preamble_p2_abbr_text = document.createTextNode("WaSP");
preamble_p2_abbr.appendChild(preamble_p2_abbr_text);
preamble_p2.appendChild(premble_p2_text1);
preamble_p2.appendChild(preamble_p2_abbr);
preamble_p2.appendChild(premble_p2_text2);

let preamble_p3 = document.createElement("p");
let preamble_p3_text = document.createTextNode("The CSS Zen Garden invites you to relax and meditate on the important lessons of the masters. Begin to see with clarity. Learn to use the time-honored techniques in new and invigorating fashion. Become one with the web.");
preamble_p3.appendChild(preamble_p3_text);

preamble.appendChild(preamble_h3);
preamble.appendChild(preamble_p1);
preamble.appendChild(preamble_p2);
preamble.appendChild(preamble_p3);

intro.appendChild(banner);
intro.appendChild(summary);
intro.appendChild(preamble);
// ---------zen intro section ends----------\\

// ---------zen-supporting section starts--------\\
main_supporting.setAttribute("class","main supporting");
main_supporting.setAttribute("id","zen-supporting");
main_supporting.setAttribute("role","main");

// explanation area
let explanation = document.createElement("div");
explanation.setAttribute("class","explanation");
explanation.setAttribute("id","zen-explanation");
explanation.setAttribute("role","article");

let explanation_h3 = document.createElement("h3");
let explanation_h3_text = document.createTextNode("So What is This About?");
explanation_h3.appendChild(explanation_h3_text);

let explanation_p1 = document.createElement("p");
let explanation_p1_text1 = document.createTextNode(". There is a continuing need to show the power of ");
let explanation_p1_text2 = document.createTextNode("The Zen Garden aims to excite, inspire, and encourage participation. To begin, view some of the existing designs in the list.Clicking on any one will load the style sheet into this very page. The ");
let explanation_p1_text3 = document.createTextNode("remains the same, the only thing that has changed is the external ");
let explanation_p1_text4 = document.createTextNode(" file. Yes, really.");

let explanation_p1_abbr1 = document.createElement("abbr");
explanation_p1_abbr1.setAttribute("title","Cascading Style Sheets");
explanation_p1_abbr1.appendChild(abbr_text);

let explanation_p1_abbr2 = document.createElement("abbr");
explanation_p1_abbr2.setAttribute("title","HyperText Markup Language");
let explanation_p1_abbr2_text = document.createTextNode("HTML");
explanation_p1_abbr1.appendChild(explanation_p1_abbr2_text);

let explanation_p1_abbr3 = document.createElement("abbr");
explanation_p1_abbr3.setAttribute("title","Cascading Style Sheets");
explanation_p1_abbr3.appendChild(abbr_text);

explanation_p1.appendChild(explanation_p1_text1);
explanation_p1.appendChild(explanation_p1_abbr1);
explanation_p1.appendChild(explanation_p1_text2);
explanation_p1.appendChild(explanation_p1_abbr2);
explanation_p1.appendChild(explanation_p1_text3);
explanation_p1.appendChild(explanation_p1_abbr3);
explanation_p1.appendChild(explanation_p1_text4);

let explanation_p2 = document.createElement("p");
let explanation_p2_text1 = document.createTextNode(` allows complete and total control over the style of a hypertext
document. The only way this can be illustrated in a way that gets people excited is by demonstrating what it can
truly be, once the reins are placed in the hands of those able to create beauty from structure. Designers and
coders alike have contributed to the beauty of the web; we can always push it further.`);
explanation_p2.appendChild(explanation_p1_abbr3);
explanation_p2.appendChild(explanation_p2_text1);

explanation.appendChild(explanation_h3);
explanation.appendChild(explanation_p1);
explanation.appendChild(explanation_p2);

// participation area
let participation = document.createElement("div");
participation.setAttribute("class","participation");
participation.setAttribute("id","zen-participation");
participation.setAttribute("role","article");

let participation_h3 = document.createElement("h3");
let participation_h3_text = document.createTextNode("Participation");
participation_h3.appendChild(participation_h3_text);

let participation_p1 = document.createElement("p");
let participation_p1_text1 = document.createTextNode("Strong visual design has always been our focus. You are modifying this page, so strong ");
let participation_p1_text2 = document.createTextNode("skills are necessary too, but the example files are commented well enough that even ");
let participation_p1_text3 = document.createTextNode("novices can use them as starting points. Please see the ");
let participation_p1_text4 = document.createTextNode(`for advanced tutorials and tips on working with `);

let participation_p1_a = document.createElement("a");
let participation_p1_a_text = document.createTextNode("a");
participation_p1_a.setAttribute("href","http://www.mezzoblue.com/zengarden/resources/");
participation_p1_a.appendChild(participation_p1_a_text);

participation_p1.appendChild(participation_p1_text1);
participation_p1.appendChild(explanation_p1_abbr3);
participation_p1.appendChild(participation_p1_text2);
participation_p1.appendChild(participation_p1_a);
participation_p1.appendChild(participation_p1_text3);
participation_p1.appendChild(explanation_p1_abbr3);
participation_p1.appendChild(participation_p1_text4);
participation_p1.appendChild(explanation_p1_abbr3);

let participation_p2 = document.createElement("p");
let participation_p2_text1 = document.createTextNode("You may modify the style sheet in any way you wish, but not the ");
let participation_p2_text2 = document.createTextNode(` This may seem daunting at first if you&#8217;ve never worked this way before, but follow the listed links to learn
more, and use the sample files as a guide.`);
explanation_p1_abbr2.appendChild(explanation_p1_abbr2_text);
participation_p2.appendChild(participation_p2_text1);
participation_p2.appendChild(explanation_p1_abbr2);
participation_p2.appendChild(participation_p2_text2);

let participation_p3 = document.createElement("p");
let participation_p3_text1 = document.createTextNode("Download the sample ");
let participation_p3_text2 = document.createTextNode(` and `);
let participation_p3_text3 = document.createTextNode(` to work on a copy locally. Once you have completed your masterpiece (and please, don&#8217;t submit half-finished work) upload your `);
let participation_p3_text4 = document.createTextNode(` file to a web server under your control. `);
let participation_p3_text5 = document.createTextNode(` to an archive of that file and all
associated assets, and if we choose to use it we will download it and place it on our server. `);

let participation_p3_a1 = document.createElement("a");
participation_p3_a1.setAttribute("href","/examples/index");
participation_p3_a1.setAttribute("title","This page's source HTML code, not to be modified.");
let participation_p3_a1_text = document.createTextNode(" HTML ");
participation_p3_a1.appendChild(participation_p3_a1_text);

let participation_p3_a2 = document.createElement("a");
participation_p3_a2.setAttribute("href","/examples/style.css");
participation_p3_a2.setAttribute("title","This page's source CSS code, not to be modified.");
let participation_p3_a2_text = document.createTextNode(" CSS ");
participation_p3_a2.appendChild(participation_p3_a2_text);

let participation_p3_a3 = document.createElement("a");
participation_p3_a3.setAttribute("href","/examples/style.css");
participation_p3_a3.setAttribute("title","This page's source CSS code, not to be modified.");
let participation_p3_a3_text = document.createTextNode(" CSS ");
participation_p3_a3.appendChild(participation_p3_a3_text);

participation_p3.appendChild(participation_p3_text1);
participation_p3.appendChild(participation_p3_a1);
participation_p3.appendChild(participation_p3_text2);
participation_p3.appendChild(participation_p3_a2);
participation_p3.appendChild(participation_p3_text3);
participation_p3.appendChild(explanation_p1_abbr3);
participation_p3.appendChild(participation_p3_text4);
participation_p3.appendChild(participation_p3_a3);
participation_p3.appendChild(participation_p3_text5);

participation.appendChild(participation_h3);
participation.appendChild(participation_p1);
participation.appendChild(participation_p2);
participation.appendChild(participation_p3);
                    
//  benefits area
let benefits = document.createElement("div");
benefits.setAttribute("class","benefits");
benefits.setAttribute("id","zen-benefits");
benefits.setAttribute("role","article");

let benefits_h3 = document.createElement("h3");
let benefits_h3_text = document.createTextNode("Benefits");
benefits_h3.appendChild(benefits_h3_text);

let benefits_p1 = document.createElement("p");
let benefits_p1_text1 = document.createTextNode(`Why participate? For recognition, inspiration, and a resource we can all refer to showing people how amazing `);
let benefits_p1_text2 = document.createTextNode(`really can be. This site serves as equal parts inspiration for those 
working on the web today, learning tool for those who will be tomorrow, and gallery of future techniques we can all look forward to.`);
benefits_p1.appendChild(benefits_p1_text1);
benefits_p1.appendChild(explanation_p1_abbr3);
benefits_p1.appendChild(benefits_p1_text2);

benefits.appendChild(benefits_h3);
benefits.appendChild(benefits_p1);

// requirements area
let requirements = document.createElement("div");
requirements.setAttribute("class","requirements");
requirements.setAttribute("id","zen-requirements");
requirements.setAttribute("role","article");

let requirements_h3 = document.createElement("h3");
let requirements_h3_text = document.createTextNode("Requirements");
requirements_h3.appendChild(benefits_h3_text);

let requirements_p1 = document.createElement("p");
let requirements_p1_text1 = document.createTextNode(`Where possible, we would like to see mostly `);
let requirements_p1_text2 = document.createTextNode(` usage. `);
let requirements_p1_text3 = document.createTextNode(` should be limited to widely-supported elements only, or strong fallbacks should be provided. The CSS Zen Garden is about functional, practical `);
let requirements_p1_text4 = document.createTextNode(` and not the latest bleeding-edge tricks viewable by 2%
of the browsing public. The only real requirement we have is that your `);
let requirements_p1_text5 = document.createTextNode(`validates. `);

let requirements_p1_abbr1 = document.createElement("abbr");
let requirements_abbr1_text = document.createTextNode(`CSS 1 &amp; 2`);
requirements_p1_abbr1.setAttribute("title","Cascading Style Sheets, levels 1 and 2");
requirements_p1_abbr1.appendChild(requirements_abbr1_text);

let requirements_p1_abbr2 = document.createElement("abbr");
let requirements_abbr2_text = document.createTextNode(`CSS 3 &amp; 4`);
requirements_p1_abbr2.setAttribute("title","Cascading Style Sheets, levels 3 and 4");
requirements_p1_abbr2.appendChild(requirements_abbr2_text);

 requirements_p1.appendChild(requirements_p1_text1);
 requirements_p1.appendChild(requirements_p1_abbr1);
 requirements_p1.appendChild(requirements_p1_text2);
 requirements_p1.appendChild(requirements_p1_abbr2);
 requirements_p1.appendChild(requirements_p1_text3);
 requirements_p1.appendChild(explanation_p1_abbr3);
 requirements_p1.appendChild(requirements_p1_text4);
                    
let requirements_p2 = document.createElement("p");
let requirements_p2_text1 = document.createTextNode(`Luckily, designing this way shows how well various browsers have implemented `);
let requirements_p2_text2 = document.createTextNode(` by now. When sticking to the guidelines you should see fairly consistent results across most modern browsers. Due
to the sheer number of user agents on the web these days &#8212; especially when you factor in mobile &#8212;
pixel-perfect layouts may not be possible across every platform. That&#8217;s okay, but do test in as many as you
can. Your design should work in at least IE9+ and the latest Chrome, Firefox, iOS and Android browsers (run by
over 90% of the population). `);
requirements_p2.appendChild(requirements_p2_text1);
requirements_p2.appendChild(explanation_p1_abbr3);
requirements_p2.appendChild(requirements_p2_text2);

let requirements_p3 = document.createElement("p");
let requirements_p3_text1 = document.createTextNode(`We ask that you submit original artwork. Please respect copyright laws. Please keep objectionable material to a
minimum, and try to incorporate unique and interesting visual themes to your work. We&#8217;re well past the point of needing another garden-related design. `);
requirements_p3.appendChild(requirements_p3_text1);

let requirements_p4 = document.createElement("p");
let requirements_p4_text1 = document.createTextNode(`This is a learning exercise as well as a demonstration. You retain full copyright on your graphics (with limited
    exceptions, see `);
let requirements_p4_text2 = document.createTextNode(` ), but we ask you release your `);
let requirements_p4_text3 = document.createTextNode(`  under a Creative Commons license identical to the`);
let requirements_p4_text4 = document.createTextNode(` so that others may learn from your work.`);

let requirements_p4_a1 = document.createElement("a");
requirements_p4_a1.setAttribute("href","http://www.mezzoblue.com/zengarden/submit/guidelines/");
let requirements_p4_a1_text = document.createTextNode(" submission guidelines ");
requirements_p4_a1.appendChild(requirements_p4_a1_text);

let requirements_p4_a2 = document.createElement("a");
requirements_p4_a2.setAttribute("href","http://creativecommons.org/licenses/by-nc-sa/3.0/");
requirements_p4_a2.setAttribute("title","View the Zen Garden's license information.");
let requirements_p4_a2_text = document.createTextNode(" one on this site ");
requirements_p4_a2.appendChild(requirements_p4_a2_text);

requirements_p4.appendChild(requirements_p4_text1);
requirements_p4.appendChild(requirements_p4_a1);
requirements_p4.appendChild(requirements_p4_text2);
requirements_p4.appendChild(explanation_p1_abbr3);
requirements_p4.appendChild(requirements_p4_text3);
requirements_p4.appendChild(requirements_p4_a2);
requirements_p4.appendChild(requirements_p4_text4);

 // explanation_p1_abbr3 explanation_p1_abbr2
                    
let requirements_p5 = document.createElement("p");
requirements_p5.setAttribute("role","contentinfo");
let requirements_p5_text1 = document.createTextNode(`By `);
let requirements_p5_text2 = document.createTextNode(`Bandwidth graciously donated by `);
let requirements_p5_text3 = document.createTextNode(`.Now available:  `);

let requirements_p5_a1 = document.createElement("a");
requirements_p5_a1.setAttribute("href","http://www.mezzoblue.com/");
let requirements_p5_a1_text = document.createTextNode("Dave Shea");
requirements_p5_a1.appendChild(requirements_p5_a1_text);

let requirements_p5_a2 = document.createElement("a");
requirements_p5_a2.setAttribute("href","http://www.mediatemple.net/");
let requirements_p5_a2_text = document.createTextNode("mediatemple");
requirements_p5_a2.appendChild(requirements_p5_a2_text);

let requirements_p5_a3 = document.createElement("a");
requirements_p5_a3.setAttribute("href","http://www.amazon.com/exec/obidos/ASIN/0321303474/mezzoblue-20/");
let requirements_p5_a3_text = document.createTextNode("Zen Garden, the book");
requirements_p5_a3.appendChild(requirements_p5_a3_text);

requirements_p5.appendChild(requirements_p5_text1);
requirements_p5.appendChild(requirements_p5_a1);
requirements_p5.appendChild(requirements_p5_text2);
requirements_p5.appendChild(requirements_p5_a2);
requirements_p5.appendChild(requirements_p5_text3);
requirements_p5.appendChild(requirements_p5_a3);

requirements.appendChild(requirements_h3);
requirements.appendChild(requirements_p1);
requirements.appendChild(requirements_p2);
requirements.appendChild(requirements_p3);
requirements.appendChild(requirements_p4);
requirements.appendChild(requirements_p5);

// footer section
let footer = document.createElement("footer");

let footer_a1 = document.createElement("a");
footer_a1.setAttribute("href","http://validator.w3.org/check/referer");
footer_a1.setAttribute("title","Check the validity of this site&#8217;s HTML");
footer_a1.setAttribute("class","zen-validate-html");
let footer_a1_text = document.createTextNode("HTML");
footer_a1.appendChild(footer_a1_text);

let footer_a2 = document.createElement("a");
footer_a2.setAttribute("href","http://jigsaw.w3.org/css-validator/check/referer");
footer_a2.setAttribute("title","Check the validity of this site&#8217;s CSS");
footer_a2.setAttribute("class","zen-validate-css");
let footer_a2_text = document.createTextNode("CSS");
footer_a2.appendChild(footer_a2_text);

let footer_a3 = document.createElement("a");
footer_a3.setAttribute("href","http://creativecommons.org/licenses/by-nc-sa/3.0/");
footer_a3.setAttribute("title","View the Creative Commons license of this site: Attribution-NonCommercial-ShareAlike.");
footer_a3.setAttribute("class","zen-license");
let footer_a3_text = document.createTextNode("CC");
footer_a3.appendChild(footer_a3_text);

let footer_a4 = document.createElement("a");
footer_a4.setAttribute("href","http://mezzoblue.com/zengarden/faq/#aaa");
footer_a4.setAttribute("title","Read about the accessibility of this site");
footer_a4.setAttribute("class","zen-accessibility");
let footer_a4_text = document.createTextNode("Ally");
footer_a4.appendChild(footer_a4_text);

let footer_a5 = document.createElement("a");
footer_a5.setAttribute("href","https://github.com/mezzoblue/csszengarden.com");
footer_a5.setAttribute("title","Fork this site on Github");
footer_a5.setAttribute("class","zen-github");
let footer_a5_text = document.createTextNode("CH");
footer_a5.appendChild(footer_a5_text);

footer.appendChild(footer_a1);
footer.appendChild(footer_a2);
footer.appendChild(footer_a3);
footer.appendChild(footer_a4);
footer.appendChild(footer_a5);

// app
main_supporting.appendChild(explanation);
main_supporting.appendChild(participation);
main_supporting.appendChild(benefits);
main_supporting.appendChild(requirements);
main_supporting.appendChild(footer);
//--------- zen-supporting section ends--------\\

//--------- zen-aside section start--------\\
aside.setAttribute("class","sidebar");
aside.setAttribute("role","complementary");

let aside_wrapper = document.createElement("div");
aside_wrapper.setAttribute("class","wrapper");

// design-selection
let design_selection = document.createElement("div");
design_selection.setAttribute("class","design-selection");
design_selection.setAttribute("id","design-selection");

let design_selection_h3 = document.createElement("h3");
design_selection_h3.setAttribute("class","select");
let design_selection_h3_text = document.createTextNode("Select a Design:");
design_selection_h3.appendChild(design_selection_h3_text);
{/* <li>
    <a href="/221/" class="design-name">Mid Century Modern</a> by <a href="http://andrewlohman.com/" class="designer-name">Andrew Lohman</a>
</li> */}
let design_selection_nav = document.createElement("nav");
design_selection_nav.setAttribute("role","navigation");
let design_selection_nav_ul = document.createElement("ul");
let design_selection_nav_ul_li = document.createElement("li");
let design_selection_nav_ul_li_text = document.createTextNode(" by ");

let design_selection_nav_ul_li_a1 = document.createElement("a");
design_selection_nav_ul_li_a1.setAttribute("href","/221/");
design_selection_nav_ul_li_a1.setAttribute("class","design-name");
let design_selection_nav_ul_li_a1_text = document.createTextNode("Mid Century Modern");
design_selection_nav_ul_li_a1.appendChild(design_selection_nav_ul_li_a1_text);

let design_selection_nav_ul_li_a2 = document.createElement("a");
design_selection_nav_ul_li_a2.setAttribute("href","http://andrewlohman.com/");
design_selection_nav_ul_li_a2.setAttribute("class","design-name");
let design_selection_nav_ul_li_a2_text = document.createTextNode("Andrew Lohman");
design_selection_nav_ul_li_a2.appendChild(design_selection_nav_ul_li_a2_text);

design_selection_nav_ul_li.appendChild(design_selection_nav_ul_li_a1);
design_selection_nav_ul_li.appendChild(design_selection_nav_ul_li_text);
design_selection_nav_ul_li.appendChild(design_selection_nav_ul_li_a2);

for(let i = 0; i < 10 ; i++){
    design_selection_nav_ul.appendChild(design_selection_nav_ul_li);    
}
design_selection_nav.appendChild(design_selection_nav_ul);

design_selection.appendChild(design_selection_h3);
design_selection.appendChild(design_selection_nav);
// app
aside_wrapper.appendChild(design_selection);

aside.appendChild(aside_wrapper);
//--------- zen-aside section ends--------\\

container_div.appendChild(intro);
container_div.appendChild(main_supporting);
container_div.appendChild(aside);

container_div.setAttribute("class","page-wrapper");

document.getElementsByTagName('body')[0].setAttribute("id","css-zen-garden");
document.getElementsByTagName('body')[0].appendChild(container_div);
