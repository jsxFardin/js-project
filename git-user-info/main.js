// div
function div(idName, className) {
    let div = document.createElement('div');
    if (idName) {
        div.setAttribute('id', idName);
    }
    if (className) {
        div.setAttribute('class', className);
    }
    return div;
}
// img
function img(src) {
    let img = document.createElement('img');
    img.setAttribute('src', src);
    return img;
}
// h5
function hyperText(value, text) {
    let hyperText = document.createElement(value);
    hyperText.textContent = text;
    return hyperText;
}
// button
function button(textNode) {
    let btnTex = text(textNode);
    let button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.appendChild(btnTex);
    return button;
}
// text node
function text(value) {
    let text = document.createTextNode(value);
    return text;
}

// loading document from github api
function loadDoc() {
    let url = `https://api.github.com/users`;
    fetch(url)
        .then(response => response.json())
        .then((data) => {
            let users = eval(data);
            for (let user of users) {
                // col3
                let col3 = div('', 'col-lg-3 my-3');
                let card = div('', 'card');
                let avatar = img(user.avatar_url);
                avatar.setAttribute('class', 'card-img-top');
                let cardBody = div('', 'card-body');
                let cardTitle = hyperText('h5', user.login);
                cardTitle.setAttribute('class', 'card-title text-center');

                let cardBodyTitleContainer = div('', 'card-body');

                // card btn
                let cardBtn = button('show image');
                cardBtn.setAttribute('class', 'btn btn-primary btn-sm show');
                cardBtn.setAttribute('data-toggle', 'modal');
                cardBtn.setAttribute('id', user.avatar_url);
                cardBodyTitleContainer.appendChild(cardBtn);

                card.appendChild(avatar);
                card.appendChild(cardBody);
                card.appendChild(cardTitle);
                card.appendChild(cardBodyTitleContainer);
                col3.appendChild(card);

                document.getElementById('main-sec').appendChild(col3);
            }
            $(document).on('click', '.show', (e) => {
                document.getElementById("modal-img").src = e.target.id;

                $("#modalID").modal();
                // modal(e.target.id);
            });
        })
}
// loadDoc();

// let col12 = div('', 'col-lg-12 mt-4');
let loadBtn = button('load data');
loadBtn.setAttribute('class', 'btn btn-info');
loadBtn.setAttribute('id', 'loadDoc');

// load data by on cick show btn
// col12.append(loadBtn);
document.getElementById("main-sec1").append(loadBtn);

document.getElementById('loadDoc').addEventListener('click', loadDoc, false);

function modal() {
    let modal = div('modalID', 'modal fade');
    modal.setAttribute('tabindex', '-1');
    modal.setAttribute('role', 'dialog');
    modal.setAttribute('aria-labelledby', 'modelTitleId');
    modal.setAttribute('aria-hidden', 'true');

    let modalDailog = div('', 'modal-dialog');
    modal.setAttribute('role', 'document');

    let modalContent = div('', 'modal-content');
    let modalHeader = div('', 'modal-header');
    let modalBody = div('', 'modal-body');

    // modal-body img
    let image = img();
    image.setAttribute('id', 'modal-img');
    image.setAttribute('style', 'width:100%; height:100%');
    modalBody.appendChild(image);

    // footer
    let modalFooter = div('', 'modal-footer');

    modalContent.appendChild(modalHeader);
    modalContent.appendChild(modalBody);
    modalContent.appendChild(modalFooter);
    modalDailog.appendChild(modalContent);
    modal.appendChild(modalDailog);

    document.getElementsByTagName('body')[0].appendChild(modal);

}
modal();


