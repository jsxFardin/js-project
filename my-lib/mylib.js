// div
function div(attrs) {
    let div = document.createElement('div');
    if (attrs) {
        setAttributes(div, attrs);
    }
    return div;
}
// img
function img(attrs) {
    let img = document.createElement('img');
    if (attrs) {
        setAttributes(img, attrs);
    }
    return img;
}
// h5
function hyperText(type, text) {
    let hyperText = document.createElement(type);
    hyperText.textContent = text;
    return hyperText;
}
// button
function button(textNode) {
    let btnTex = text(textNode);
    let button = document.createElement('button');
    button.setAttribute('type', 'button');
    button.appendChild(btnTex);
    return button;
}
// text node
function text(value) {
    let text = document.createTextNode(value);
    return text;
}

// crating ul
function ul(attrs) {
    let ul = document.createElement('ul');
    if (attrs) {
        setAttributes(ul, attrs);
    }
    return ul;
}

// for set multiple attribute
function setAttributes(element, attrs) {
    for (let attr in attrs) {
        element.setAttribute(attr, attrs[attr]);
    }
    return element;
}

// input field
function input(attrs){
    let input = document.createElement('input');
    if (attrs) {
        setAttributes(input, attrs);
    }
    return input;
}